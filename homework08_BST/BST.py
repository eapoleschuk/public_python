#coding: utf-8
from sys import stdout
class Vertex(object):
    def __init__(self, k):
        self.key = k
        self.lChild = None
        self.rChild = None

class Tree(object):
    def __init__(self):
        self.root = None
        #self.vertex = None

    def createNode(self, key):
        return Vertex(key)

    def addNode(self, root, v):
        if self.root == None:
            self.root = v
        else:
            if v.key<=root.key:
                if root.lChild != None:
                    self.addNode(root.lChild, v)
                else:
                    root.lChild = v
            else:
                if root.rChild != None:
                    self.addNode(root.rChild, v)
                else:
                    root.rChild = v
    def buildTree(self, s):
        for i in s:
            v = self.createNode(i)
            self.addNode(self.root, v)

    def printTree(self, root, nbsp):
        if self.root != None:
            print "root", self.root.key
        else:
            if root.lChild != None:
                print "lChild ", root.key, "->", root.lChild.key
                self.printTree(root.lChild, nbsp+1)
            else:
                print "lChild ", root.key, "->", root.lChild
            if root.rChild != None:
                print "rChild ", root.key, "->", root.rChild.key
                self.printTree(root.rChild, nbsp+1)
            else:
                print "rChild ", root.key, "->", root.rChild.key
s = [10, 5]
v = Vertex(12)
v1 = Vertex(3)
t = Tree()
t.root = v
t.root.lChild = v1
t.buildTree(s)
t.printTree(t.root, 2)

#stdout.flush()
