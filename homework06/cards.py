#coding: utf-8
"""
    Игра немного не похожа на дурака :)
    ----------------
        Игроки ходят автоматически, происходит сравнение карт по масти, если масти разные и одна из них не козырь, то ничья.
        Если масти одинаковые масти, а номиналы разные, номиналы сравниваются, у кого больше, тот и победил в этом раунде.
        У кого больше очков по истечение всех раундов (пока не закончатся карты), то и победил:)
"""

import random
class Game(object):
    suits = [" Clubs"," Spades"," Hearts"," Diamonds"]
    rate = [2,3,4,5,6,7,8,9,10, 'jack', 'lady', 'king', 'ace']
    cards = [str(i)+j for i in rate for j in suits]
    ace = None
    def __init__(self):
        random.shuffle(self.cards)
    def init_ace(self):
        self.ace = random.choice(self.suits).split(' ')[1]
        return self.ace
    def solve(self, p1, p2):
        #self.player1 = p1
        #self.player2 = p2
        step1 = p1.doStep()
        step2 = p2.doStep()
        rs1 = step1.split(' ')
        rs2 = step2.split(' ')
        print "step1 ", step1, '\trate pl1 - ', rs1[0], ' ', rs1[1]
        print "step2 ", step2, '\trate pl2 - ', rs2[0], ' ', rs2[1]
        if rs1[1] == rs2[1]:
            if rs1[0] > rs2[0]:
                p1.score += 1
                print 'Winner - player 1'
                return p1.score
            elif rs1[0] < rs2[0]:
                p2.score += 1
                print 'Winner - player 2'
                return p2.score
            else:
                print 'No winner!'
                return 0
        elif rs1[1] == self.ace:
            p1.score += 1
            print 'Winner - player 1. Take the ace'
            return p1.score
        elif rs2[1] == self.ace:
            p2.score += 1
            print 'Winner - player 2. Take the ace'
            return p2.score
        else:
            print "Do step again!"
            return 0
    @staticmethod
    def determine_the_winner(p1, p2):
        if p2.score > p1.score:
            print "We have a winner! player2"
        elif p1.score > p2.score:
            print "We have a winner! player1"
        else:
            print "Dead heat!"

    @classmethod
    def change_num_cards(cls, x):
        if x == 36:
            cls.rate = [6,7,8,9,10, 'jack', 'lady', 'king', 'ace']
        elif x == 52:
            cls.rate = [2,3,4,5,6,7,8,9,10, 'jack', 'lady', 'king', 'ace']
        else:
            raise Exception("Не бывает столько карт!")
class Player(object):
    #cards = list()
    #score = 0
    def __init__(self, game):
        self.myGame = game
        self.cards = list()
        self.__score = 0

    @property
    def score(self):
        return self.__score
    @score.setter
    def score(self, score):
        self.__score = score
    @score.deleter
    def score(self):
        del self.__score

    def doStep(self):
        if len(self.cards)>0:
            a = random.choice(self.cards)
            for i in xrange(0,len(self.cards)-1):
                if self.cards[i] == a:
                    self.cards.pop(i)
            return a
        #else:
            #self.takeCards()
    def takeCards(self):
        if len(self.cards)<6:
           # print self.myGame.cards
            for i in xrange(0,len(self.myGame.cards)):
                self.cards.append(self.myGame.cards.pop())
                #print self.cards
                if len(self.cards)==6:
                    break
        return self.cards

Game.change_num_cards(52)
print Game.rate
game = Game()
print game.rate
player1 = Player(game)
player2 = Player(game)
print game.init_ace()

while len(game.cards)!=0:
    player1.takeCards()
    player2.takeCards()
    print player1.cards
    print player2.cards
   # print 'p1', player1.doStep()
    #print 'p2', player2.doStep()
    print game.solve(player1, player2)
    #print player1.cards
    #print player2.cards
    #game.solve(player1,player2)
Game.determine_the_winner(player1, player2)

#print game.cards
