from optparse import OptionParser
from fractions import Fraction

if __name__=="__main__":
    parser = OptionParser()
    parser.add_option("-a", "--number1", dest='a', action="store")
    parser.add_option("-b", "--number2", dest='b', action="store")

    parser.add_option("-p", "--plus", dest='plus', action="store_true")
    parser.add_option("-s", "--subtraction", dest='sub', action="store_true")
    parser.add_option("-m", "--multiply", dest='mult', action="store_true")
    parser.add_option("-d", "--divide", dest='div', action="store_true")

    (options, args) = parser.parse_args()

    a = Fraction(options.a)
    b = Fraction(options.b)

    if options.plus:
        print "{0}+{1} = {2}".format(a,b, a+b)
    elif options.sub:
        print "{0}-{1} = {2}".format(a,b, a-b)
    elif options.mult:
        print "{0}*{1} = {2}".format(a,b, a*b)
    elif options.div:
        print "{0} / {1} = {2}".format(a,b, a/b)
