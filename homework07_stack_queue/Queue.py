#coding: utf-8
import Queue
class Queue(object):

    def __init__(self, q):
        self.size = q[0]
        self.head = q[1]
        self.tail = q[2]
        self.queue = q[3:self.size+3]

    def print_queue(self):
        print "head = {0}, tail = {1}, queue = {2}".format(self.head,self.tail, self.queue)
    def check_empty(self):
        #if self.head == self.tail:
        return self.head == self.tail
        #else:
          #  return False
    def enqueue(self, value):
        #если хвост догнал голову, или массив заполнен от начала до конца
        if self.head - self.tail == 1 or (self.head == 0 and self.tail == self.size):
            raise Exception("Queue overflow")
        else:
            if self.tail == self.size:
                self.tail = 0
                self.queue[self.tail] = value
            else:
                self.queue[self.tail] = value
                self.tail += 1
        self.print_queue()

    def dequeue(self):
        if self.check_empty():
            print "Queue is empty"
        else:
            value = self.queue[self.head]
            if self.head == self.size:
                self.head = 0
            else:
                self.head += 1
            return value



q = [4, 0, 0, 3, 5, 5, 5]
b = 4
queue = Queue(q)
queue.print_queue()
queue.enqueue(0)
queue.enqueue(2)
queue.enqueue(3)
queue.enqueue(4)
print queue.dequeue()
queue.print_queue()
queue.enqueue(1)
