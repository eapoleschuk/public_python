#coding:utf-8
class Stack(object):
    """
    Проблема: если массив пуст, то заполнить его не получается, т.к. я вставляю эелементы стека по поределенному индексу,
    а так как такого индекса не существует, выбрасывается исключение  self.stack[self.top] = value
    IndexError: list assignment index out of range
    """
    def __init__(self, s):
        self.top = s[0]
        self.size = s[1]
        self.__stack = s[2:self.size+2]
    #def __len__(self):
    @property
    def stack (self):
        return self.__stack
    @stack.setter
    def stack (self, s):
        self.__stack = s #? правильно ли так делать или так только ссылка хранится в другой переменной?
    @stack.deleter
    def stack(self):
        del self.__stack
    def stack_empty(self):
        if self.top == 0:
            return True
        else:
            return False
    def pop(self):
        if self.stack_empty():
            print "Stack is empty!"
        else:
            value = self.stack[self.top]
            self.top -= 1
            return value
    def push(self, value):
        if self.top == self.size:
            raise Exception("Stack overflow")
        else:
            self.stack[self.top] = value
            self.top +=1
            return True


s = [0,5,0,1,2,3,4]
a = 6
stack = Stack(s)
#print stack.top, stack.size, stack.stack]
print stack.top
print stack.pop(), stack.top
print stack.pop(), stack.top
print stack.pop(), stack.top
print stack.pop(), stack.top
stack.push(a)
print stack.top, stack.stack
stack.push(a)
print stack.top, stack.stack
stack.push(a)
print stack.top, stack.stack
stack.push(a)
print stack.top, stack.stack