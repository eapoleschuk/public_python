# coding:utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import sqlite3


def gen(word):
    for key in word.keys():
        if key != 'trans' and key != 'word':
            for val in word[key]:
                yield key, val
        else:
            yield key, word[key]


def voc(word, *arr):
    sun = {'trans': 'сан', 'noun': ['солнце', 'солнечный луч'], 'verb': ['загорать', 'греть на солнце'],
           'adj': []}
    like = {'trans': 'лайк', 'noun': ['', ''], 'verb': ['', ''], 'adj': []}
    if word == 'sun':
        for i in gen(sun):
            yield i[0], i[1]


for i in voc('sun'):
    print i[0], i[1]
